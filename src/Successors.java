public class Successors {
    public static Position findPosition(int num, int[][] intArr) {
        for (int r = 0; r < intArr.length; r++) {
            for (int c = 0; c < intArr[r].length; c++) {
                if (intArr[r][c] == num) {
                    return new Position(r, c);
                }
            }
        }
        return null;
    }

    public static Position[][] getSucccessorArray(int[][] intArr) {
        int rows = intArr.length;
        int cols = intArr[0].length;
        Position[][] posArr = new Position[rows][cols];

        for (int r = 0; r < intArr.length; r++) {
            for (int c = 0; c < intArr[r].length; c++) {
                posArr[r][c] = Successors.findPosition(intArr[r][c] + 1, intArr);
            }
        }
        return posArr;
    }
}
