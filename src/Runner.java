public class Runner {
    public static void main(String[] args) {
        int[][] multiDimension = {
                {15, 5, 9, 10},
                {12, 16, 11, 6},
                {14, 8, 13, 7}
        };

        Position[][] successors = Successors.getSucccessorArray(multiDimension);

        for (int r = 0; r < successors.length; r++) {
            for (int c = 0; c < successors[r].length; c++) {
                System.out.print(successors[r][c]);
            }
            System.out.println();
        }
    }
}
